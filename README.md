## TLDR;
PHP библиотека за валидиране на входни данни. 

```php
<?php
$validator = new Carbon_Validator($_POST, array(
	'name'    => 'required',
	'email'   => 'required|email',
	'message' => 'string_min_length:15|string_max_length:255'
));

if ($validator->fails()) {
	$errors = $validator->get_errors();
} else {
	send_mail_notification();
	header('Location: ?success');
}
?>
```

 1. Създаваме нов обект като подаваме входните данни и правилата според които трябва да се валидират тези данни
  1. Входните данни трябва да бъдат асоциативен масив подобен на `$_POST`
  1. Правилата за валидация са отново асоциативен масив в който ключа е името на полето а стойността: всички правила, които се отнасят към него
 1. Можем да проверим дали данните са валидни като извикаме метода `$validator->fails()` или `$validator->passes()`
 1. Можем да извлечем автоматично генериран списък от грешки от валидатора чрез `$validator->get_errors()`
 1. Можем да подаваме специфични грешки за определени полета и правила(пример по-долу)

------

## Дефиниция на правилата

Всяко поле се асоциира с набор от валидационни правила. Той може да бъде пуснат под формата на string или масив. Когато правилата се подават като стринг те се разделят от символа pipe(`|`) по следния начин:

```php
<?php
$validator = new Carbon_Validator($_POST, array(
	// The "customer_email" field is required and must be valid email
	'customer_email' => 'required|email' 
));
?>
```

Когато се подават като масив той трябва да е числово индексиран по следния начин:

```php
<?php
$validator = new Carbon_Validator($_POST, array(
	'email' => array('required', 'email')
));
?>
```

Всяко правило само по себе си се състои от:

 1. Име(например `string_min_length`)
 1. Аргумент разделен от името с двуеточие(например `:15` от `string_min_length:15`)

----

## Налични правила за валидиране

`Carbon_Validator` идва с няколко пакето от валидационни правила. За да заредите пакет се използва следния статичен метод:
```php
Carbon_Validator::load_package('wordpress');
```

Пакет `generic` (зарежда се по подразбиране):
 * `required` - полето трябва да има някаква стойност
 * `in:value1,value2,value3,...` - полето трябва да има една от стойностите подадени като параметри на правилото
 * `email` - валиден email адрес (зад кулисите се използва функцията `filter_var` на PHP)
 * `regex:/regex_body/i` - валидира стойността на полето с подадения регулярен израз
 * `numeric` - прекарва стойността през вградената в PHP `is_numeric` функция
 * `string_min_length:value` - указва минимална дължина на полето
 * `string_max_length:value` - указва максимална дължина на полето
 * `numeric_min:value` - указва минимална числова стойност на полето
 * `numeric_max:value` - указва максимална числова стойност на полето
 * `url` - валиден URL адрес(зад кулисите се използва функцията `filter_var` на PHP)
 * `file:ext,ext2` - уверява се че е качен файл с опеделен тип. Не се прави проверка на самия extension на файа, а на MIME типа му
 * `filesize:2M` - уверява се че е качен файл под определен размер(2MB). Приемат се и размери със суфикс *K*(от KB)
 * `email_exists` - проверява дали вече съществува потребител с този email адрес
 * `user_exists` - проверява дали вече съществува потребител с този username

Пакет `wordpress`:
 * `wp_nonce:action_name` - Валидира полето чрез `wp_verify_nonce()`
 * `wp_post:post_type1,post_type2` - Валидира `ID` на пост чрез `get_post()`. По избор можете да добавите 1 или повече пост типове като изискване за валидация на поста
 * `wp_user:role_or_capability1,role_or_capability2` - Валидира `ID` на потребител чрез `get_userdata()`. По избор можете да добавите 1 или повече роли/възможности като изискване за валидация на потребителя

---

## Store / Load

Имате възможност да запазвате инстанции на Carbon_Validator и да ги заредите по-късно.

```php
Carbon_Validator::store('some_id', $validator);

$validator = Carbon_Validator::load('some_id');
```

### Example:

**functions.php**

```php
<?php
add_action('template_redirect', 'crb_handle_contact_form');
function crb_handle_contact_form() {
	$is_contact_request = isset($_POST['action']) && $_POST['action'] === 'contact_form';

	if (!$is_contact_request) {
		return false;
	}

	$validator = new Carbon_Validator($_POST, array(
		"contact_name" => "required",
		"contact_email" => "required|email",
		"contact_message" => "required",
	));

	// Store the validator, so we can use it in the front-end
	Carbon_Validator::store('contact_form' , $validator);

	$success = $validator->passes();

	if ($success) {
		// Send email notification, or whatever. 
	}
}
?>
```

**templates/contact.php**

```php
<?php
// Load the validator that was previously stored in functions.php
$validator = Carbon_Validator::load('contact_form');

// Check if the request is successful, otherwise output the errors
if ($validator): ?>
	<?php if ($validator->passes()): ?>
		<p class="alert alert-success"><?php _e('Your message has been sent.', 'crb'); ?></p> 
	<?php elseif ($errors = $validator->get_errors()): ?>
		<div class="alert alert-danger">
			<ul>
				<?php foreach ($errors as $error): ?>
					<li><?php echo esc_html($error) ?></li>
				<?php endforeach ?>
			</ul>
		</div>
	 <?php endif ?>
<?php endif; ?>
```

----

## Custom Error Messages
Автоматично генерираните валидационни грешки вършат работа в повечето ситуации. Понякога обаче се налага да се добавят и custom error messages. Това се прави чрез 3ти аргумент на конструктора на класа `Validator`:

```php
<?php
$rules = array(
	'name'    => 'required',
	'email'   => 'required|email',
	'message' => 'string_min_length:15|string_max_length:255'
);

$error_messages = array(
	'name.required' => 'We need your full name in order to contact you later. ',
	'message.string_min_length' => 'Your message is too short -- please enter more than 15 characters. ',
	'message.string_max_length' => 'Your message is too long -- please enter less than 255 characters. '
);

$validator = new Carbon_Validator($_POST, $rules, $error_messages);
?>
```

### Global default error messages

За да добавите или презапишете едно съобщение за всички инстанции на Carbon_Validator, можете да използвате следния статичен метод:
```php
// Carbon_Validator::register_default_error_message($rule, $message);
Carbon_Validator::register_default_error_message('required', 'You should really fill out the "%1$s" field.');
```
*Ако го използвате, `%1$s` ще бъде заместено с user-friendly име на полето, което се валидира.*

Този метод също ви позволява да преминете дадено съобщение през функция за превеждане ( `__()`, `_е()` и т.н. в WordPress)

----

## Custom Validation Rules
Вградените правила не винаги са достатъчни. Когато трябва да се направи по-специфична валидация можем да напраим следното:

```php
<?php
/**
 * Validates IP address and makes sure that it matches format "192.168.XXX.XXX"
 */
function validate_internal_ip_address($value) {
    if (!filter_var($value, FILTER_VALIDATE_IP)) {
    	return false;
    }
    $pieces = explode('.', $value);

    if (count($pieces) !== 4) {
    	return false;
    }

    if (intval($pieces[0]) !== 192 || intval($pieces[1]) !== 168) {
        return false;
    }

    return true;
}

// Bind the "internal_ip" validation rule to the "validate_internal_ip_address" callback function
Carbon_Validator::extend('internal_ip', 'validate_internal_ip_address');

# $_POST looks like:
# array(
# 	"name" => "John Doe",
# 	"ip-address" => "192.168.2.54",
# );

$rules = array(
	'ip-address' => 'required|internal_ip',
);

$messages = array(
	'ip-address.internal_ip' => 'Please enter valid internal IP addres. ',
);

$validator = new Carbon_Validator($_POST, $rules, $messages);
?>
```

Разширяването става чрез статичния метод `Carbon_Validator::extend($rule_name, $callback)`. След разширяване, `$rule_name` може да се ползва във валидационните правила на формите. 

Самите грешки могат да се се предават към `Carbon_Validator` класа чрез хвърляне на специално изключение `Carbon_Validator_ValidationError`.

### Custom Validation Rule Callback

Параметрите, които се подават на callback-а са:
```php
function my_custom_validation_rule_callback($value, $parameters, $field, $validator_instance) { };
```

1. `$value` - стойността, която трябва да се валидира
1. `$parameters` - масив от стойности подадени като параметри за правилото (в списък разделен със запетаи)
1. `$field` - полето, което се валидира
1. `$validator_instance` - референция към инстанцията на валидатора, която вика този callback

-----

## Валидиране на файлове

```php
<?php
$data = array_merge($_POST, array(
	'avatar' => Carbon_FileUpload::make($_FILES['avatar'])
));

$validator = new Carbon_Validator($data, array(
    'name' => 'required',
    'email' => 'required|email',
    'message' => 'required',
    'avatar' => 'file:jpg|filesize:1M',
));

?>
```
Наличните разширения за валидиране на файлове могат да бъдат отркти във `validator/mime-types.php`. 

------

## Валидиране на масиви
Когато имате масиви в `$_POST` можете да валидирате определен елемент на като използвате квадратни скоби за да опишете до него по следния начин:

```php
<?php
$input = array(
	'user' => array(
		'first_name' => 'John',
		'last_name' => 'Doe',
		'email' => 'johndoe@example.com',
		'address' => array(
			'street' => array(
				'number' => "123"
			)
		)
	)
);
$validator = new Carbon_Validator($input, array(
	'user[first_name]' => 'required',
	'user[last_name]' => 'required|string_min_length:2',
	'user[email]' => 'email',
	'user[address][street][number]' => 'required',
));

var_dump($validator->passes());
?>
```

----

Дизайна на класа е взаимстван от библиотеката [Laravel](http://laravel.com/docs/5.0/validation).
